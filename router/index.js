import express from 'express';
import json from 'body-parser';
import alumnosDb from '../models/alumnos.js';

export const router = express.Router();


//declarar primer ruta por omision
router.get('/',(req,res)=>{


    res.render('index',{titulo:"Mis practicas js", nombre:"Emilio Flores Licea"})
})

router.get('/tabla', (req,res)=>{
    //parametros
    const params = {
        numero:req.query.numero
    }
    res.render('tabla',params);
})

router.post('/tabla', (req,res)=>{
    //parametros
    const params = {
        numero:req.body.numero
    }
    res.render('tabla',params);
})

router.get('/cotizacion', (req,res)=>{
    //parametros
    const params = {
        valor:req.query.valor,
        pinicial:req.query.pinicial,
        plazos:req.query.plazos
    }
    res.render('cotizacion',params);
})

router.post('/cotizacion', (req,res)=>{
    //parametros
    const params = {
        valor:req.body.valor,
        pinicial:req.body.pinicial,
        plazos:req.body.plazos
    }
    res.render('cotizacion',params);
})

let rows;

router.get('/alumnos', async(req,res)=>{
    
    rows = await alumnosDb.mostrarTodos();
    res.render('alumnos',{reg:rows});


})

let params;
router.post('/alumnos', async(req,res)=>{

    try{
        params={
            Matricula:req.body.Matricula,
            Nombre:req.body.Nombre,
            Domicilio:req.body.Domicilio,
            Sexo:req.body.Sexo,
            Especialidad:req.body.Especialidad
        }
        const res = await alumnosDb.insertar(params);

    } catch(error){
        console.error(error)
        res.status(400).send("Sucedio un error:" + error);
    }

    rows = await alumnosDb.mostrarTodos();
    res.render('alumnos',{reg:rows});
})



export default {router}
